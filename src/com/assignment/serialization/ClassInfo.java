package com.assignment.serialization;

import java.io.Serializable;

public class ClassInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2964974869104932743L;

	private String grade;
	private String roll;
	private String school;

	public ClassInfo(String grade, String roll, String school) {
		super();
		this.grade = grade;
		this.roll = roll;
		this.school = school;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getRoll() {
		return roll;
	}

	public void setRoll(String roll) {
		this.roll = roll;
	}

	public String getSchool() {
		return school;
	}

	public void setSchool(String school) {
		this.school = school;
	}

	public void display() {
		System.out.println(toString());
	}

	@Override
	public String toString() {
		return "ClassInfo [grade=" + grade + ", roll=" + roll + ", school=" + school + "]";
	}

}
