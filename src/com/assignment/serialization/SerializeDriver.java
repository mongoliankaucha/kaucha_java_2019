package com.assignment.serialization;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Scanner;

public class SerializeDriver {
	static String name;
	static String address;
	static String dob;
	static String grade;
	static String roll;
	static String school;

	public static void main(String[] args) {
		getClassData();
		getPersonData();
		ClassInfo classInfo = new ClassInfo(grade, roll, school);
		DetailInfo detailInfo = new DetailInfo(name, address, dob, classInfo);
		detailInfo.display();
		classInfo.display();

		// Serialization
		try {
			FileOutputStream fileOut = new FileOutputStream("information.ser");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(detailInfo);

			// out.flush();
			out.close();
			// fileOut.flush();
			fileOut.close();

			System.out.printf("Serialized data is saved as information.ser");
		} catch (IOException i) {
			i.printStackTrace();
		}
	}

	private static void getClassData() {
		// TODO Auto-generated method stub
		System.out.println("class:");
		grade = getScanner().nextLine();
		System.out.println("roll:");
		roll = getScanner().nextLine();
		System.out.println("school:");
		school = getScanner().nextLine();

	}

	private static void getPersonData() {
		// TODO Auto-generated method stub
		System.out.println("name:");
		name = getScanner().nextLine();
		System.out.println("address:");
		address = getScanner().nextLine();
		System.out.println("DOB:");
		dob = getScanner().nextLine();

	}

	private static Scanner getScanner() {
		// TODO Auto-generated method stub
		return new Scanner(System.in);
	}
}
