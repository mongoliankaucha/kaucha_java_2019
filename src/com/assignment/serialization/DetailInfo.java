package com.assignment.serialization;

import java.io.Serializable;

public class DetailInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3317543465977496338L;

	private String name;
	private String address;
	private String dob;
	ClassInfo classInfo;

	public DetailInfo() {

	}

	public DetailInfo(String name, String address, String dob) {

		this.name = name;
		this.address = address;
		this.dob = dob;
	}

	public DetailInfo(String name, String address, String dob, ClassInfo classInfo) {

		this.name = name;
		this.address = address;
		this.dob = dob;
		this.classInfo = classInfo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public ClassInfo getClassInfo() {
		return classInfo;
	}

	public void setClassInfo(ClassInfo classInfo) {
		this.classInfo = classInfo;
	}

	public void display() {
		// TODO Auto-generated method stub
		System.out.println(toString());
	}

	@Override
	public String toString() {
		return "DetailInfo [name=" + name + ", address=" + address + ", dob=" + dob + ", classInfo=" + classInfo + "]";
	}

}
