package com.assignment.bank.transaction;

public class WireTransfer extends Transaction {

	private String beneficiaryFirstName;
	private String beneficiaryLastName;
	private String intermediateBankSWIFTCode;
	private String beneficiaryBankName;
	private String beneficiaryAccountNumber;

	public String getBeneficiaryFirstName() {
		return beneficiaryFirstName;
	}

	public void setBeneficiaryFirstName(String beneficiaryFirstName) {
		this.beneficiaryFirstName = beneficiaryFirstName;
	}

	public String getBeneficiaryLastName() {
		return beneficiaryLastName;
	}

	public void setBeneficiaryLastName(String beneficiaryLastName) {
		this.beneficiaryLastName = beneficiaryLastName;
	}

	public String getIntermediateBankSWIFTCode() {
		return intermediateBankSWIFTCode;
	}

	public void setIntermediateBankSWIFTCode(String intermediateBankSWIFTCode) {
		this.intermediateBankSWIFTCode = intermediateBankSWIFTCode;
	}

	public String getBeneficiaryBankName() {
		return beneficiaryBankName;
	}

	public void setBeneficiaryBankName(String beneficiaryBankName) {
		this.beneficiaryBankName = beneficiaryBankName;
	}

	public String getBeneficiaryAccountNumber() {
		return beneficiaryAccountNumber;
	}

	public void setBeneficiaryAccountNumber(String beneficiaryAccountNumber) {
		this.beneficiaryAccountNumber = beneficiaryAccountNumber;
	}

}
