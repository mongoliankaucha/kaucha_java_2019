package com.assignment.bank.transaction;

import com.assignment.bank.enumConcept.CheckType;

public class ElectronicCheck extends Transaction {
	private CheckType checkType;

	public CheckType getCheckType() {
		return checkType;
	}

	public void setCheckType(CheckType checkType) {
		this.checkType = checkType;
	}

}
