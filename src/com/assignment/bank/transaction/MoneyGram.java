package com.assignment.bank.transaction;

import com.assignment.bank.enumConcept.DeliveryOption;

public class MoneyGram extends Transaction {

	private DeliveryOption deliveryOption;
	private String destinationCountry;

	public DeliveryOption getDeliveryOption() {
		return deliveryOption;
	}

	public void setDeliveryOption(DeliveryOption deliveryOption) {
		this.deliveryOption = deliveryOption;
	}

	public String getDestinationCountry() {
		return destinationCountry;
	}

	public void setDestinationCountry(String destinationCountry) {
		this.destinationCountry = destinationCountry;
	}

}
