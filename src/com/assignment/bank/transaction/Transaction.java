package com.assignment.bank.transaction;

public abstract class Transaction {

	private double amount;
	private double extraCharge;
	private String receiverFirstName;
	private String receiverLastName;

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getCharge() {
		return extraCharge;
	}

	public void setCharge(double charge) {
		this.extraCharge = charge;
	}

	public String getReceiverFirstName() {
		return receiverFirstName;
	}

	public void setReceiverFirstName(String receiverFirstName) {
		this.receiverFirstName = receiverFirstName;
	}

	public String getReceiverLastName() {
		return receiverLastName;
	}

	public void setReceiverLastName(String receiverLastName) {
		this.receiverLastName = receiverLastName;
	}

	public void createTransaction() {
		// TODO create transaction

	}

	public void deductAccountBalance() {
		// TODO add deduct logic

	}

}
