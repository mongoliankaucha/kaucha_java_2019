package com.assignment.bank.controller;

import java.util.List;

import com.assignment.bank.model.Account;
import com.assignment.bank.model.Customer;
import com.assignment.bank.transaction.Transaction;
import com.assignment.bank.model.Bank;

public class BankingInterfaceImpl implements BankingInterface {

	public final static Bank bank;

	static {
		bank = new Bank();
	}
	@Override
	public void addCustomer(Customer customer) {
		// TODO Auto-generated method stub
		bank.getCustomers().add(customer);
	}

	@Override
	public boolean openAccount(Customer customer, Account account) {
		// TODO Auto-generated method stub
		if (bank.getCustomers().contains(customer)) {// Check if the customer exist in the Bank!
			int i = bank.getCustomers().indexOf(customer);

			bank.getCustomers().get(i).addAccount(account);

			return true;
		} else {
			System.out.println("Customer do not exist!");

			return false;
		}

	}

	@Override
	public void sendMoney(Customer customer, Transaction transaction) {
		// TODO Auto-generated method stub

	}

	@Override
	public void depositMoneyInCustomerAccount(Customer customer) {
		// TODO Auto-generated method stub

	}

	@Override
	public void editCustomerInfo(Customer customer) {
		// TODO Auto-generated method stub

	}

	@Override
	public Customer getCustomerInfo(String ssn) {
		// TODO Auto-generated method stub
		return bank.getCustomer(ssn);
	}

	@Override
	public void printBankStatus() {
		// TODO Auto-generated method stub
		System.out.println(bank);
	}

	@Override
	public void serializeBank() {
		// TODO Auto-generated method stub

	}

	@Override
	public List<Customer> getCustomersByState(String StateCode) {
		// TODO Auto-generated method stub
		return null;
	}

}
