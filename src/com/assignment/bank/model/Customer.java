package com.assignment.bank.model;

import java.util.ArrayList;

public class Customer {
	private String firstName;
	private String lastName;
	private ArrayList<Account> accounts;
	private String address;
	private String ssn;
	private String dob;

	public Customer() {

	}

	public Customer(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;

		if (this.accounts == null) {
			this.accounts = new ArrayList<>();
		}
	}

	public Customer(String firstName, String lastName, String ssn, String address) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.ssn = ssn;
		this.address = address;

		if (this.accounts == null) {
			this.accounts = new ArrayList<>();
		}
	}

	public Customer(String firstName, String lastName, String ssn, String address, String dob) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.ssn = ssn;
		this.dob = dob;

		if (this.accounts == null) {
			this.accounts = new ArrayList<>();
		}
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public ArrayList<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(ArrayList<Account> accounts) {
		this.accounts = accounts;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public void addAccount(Account account) {
		// TODO Auto-generated method stub
		this.accounts.add(account);
	}

	public void printCustomerDetils() {
		System.out.println(toString());
	}

	@Override // annotation
	public String toString() {
		return "Customer [firstName=" + firstName + ", lastName=" + lastName + ", accounts=" + accounts + ", address="
				+ address + ", ssn=" + ssn + ", dob=" + dob + "]";
	}

}
