package com.assignment.bank.model;

import java.util.ArrayList;


import com.assignment.bank.model.Customer;

public class Bank {
	private final String BANK_NAME = "Nepal Bank";
	private ArrayList<Customer> customers; 

	public Bank() {
		this.customers = new ArrayList<>();
	}

	public Bank(ArrayList<Customer> customers) {
		this.customers = customers;
	}

	public ArrayList<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(ArrayList<Customer> customers) {
		this.customers = customers;
	}

	public String getBankName() {
		return BANK_NAME;
	}

	public void customerAdd(Customer customer) {
		this.customers.add(customer);
	}

	public void printBankStatus() {
		System.out.println(toString());
	}

	public void editCustomerCustomerInfo(Customer customer) {
		// TODO Add edit Customer logic
	}
	
	public void printBankName(){
		System.out.println(getBankName());
	}

	@Override
	public String toString() {
		return "Bank [bankName=" + BANK_NAME + ", customers=" + customers + "]";
	}

	public Customer getCustomer(String ssn) {
		// TODO Auto-generated method stub
		for(Customer customer: customers) {
			if(ssn.equals(customer.getSsn())) {
				return customer;
			}
		}
		
		return null;
	}

}
