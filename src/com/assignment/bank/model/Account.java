package com.assignment.bank.model;

import java.util.Date;

import com.assignment.bank.enumConcept.AccountType;

public class Account {

	private AccountType accountType;
	private Date accountOpenDate;
	private Date accountCloseDate;
	private double amount;

	public Account() {
		this.accountType = null;
		this.accountOpenDate = null;
		this.amount = 0;
	}

	public Account(AccountType accountType, Date accountOpenDate, double amount) {
		this.accountType = accountType;
		this.accountOpenDate = accountOpenDate;
		this.amount = amount;
	}


	public AccountType getAccountType() {
		return accountType;
	}

	public void setAccountType(AccountType accountType) {
		this.accountType = accountType;
	}

	public Date getAccountOpenDate() {
		return accountOpenDate;
	}

	public void setAccountOpenDate(Date accountOpenDate) {
		this.accountOpenDate = accountOpenDate;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public void sendMoney() {
		// TODO sendMoney service call
		// TODO once above action is successul, adjust amountBalance

	}

	@Override
	public String toString() {
		return "Account [accountType=" + accountType + ", accountOpenDate=" + accountOpenDate + ", accountCloseDate="
				+ accountCloseDate + ", amount=" + amount + "]";
	}

}
