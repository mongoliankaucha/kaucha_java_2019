package com.assignment.bank.client;

import java.util.Date;
import java.util.Scanner;

import com.assignment.bank.controller.BankingInterface;
import com.assignment.bank.controller.BankingInterfaceImpl;
import com.assignment.bank.enumConcept.AccountType;
import com.assignment.bank.model.Account;
//import com.assignment.bank.model.Account;
import com.assignment.bank.model.Customer;

public class BankUI {
	private BankingInterface bankingInterface; // member-variable or instance variable

	public static void main(String[] args) {
		BankUI bankUI = new BankUI();
		bankUI.setBankingInterface(new BankingInterfaceImpl());

		while (true) {
			displayMainMenu();
			bankUI.performMenuActionBasedOnMenuChoice();
		}
	}

	// int option=getScanner().nextInt();

	private static void displayMainMenu() {
		System.out.println("Enter 1 to add customer");
		System.out.println("Enter 2 to add account");
		System.out.println("Enter 3 to send money");
		System.out.println("Enter 4 to print bank status");
		System.out.println("Enter 5 to exit ");

	}

	private void performMenuActionBasedOnMenuChoice() {
		while (true) {
			try {
				switch (getScanner().nextInt()) {
				case 1:
					addCustomer();
					break;

				case 2:
					addCustomerAccount();
					break;

				case 3:
					sendMoney();
					break;

				case 4:
					bankingInterface.printBankStatus();
					break;

				case 5:
					System.exit(0);
					break;

				default:
					break;
				}
				break;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				// System.out.println(e.getMessage());
				System.out.println("error");
			}
		}
	}

	private void addCustomer() {
		Scanner customerInput = getScanner(); // customerInput is a local variable
		System.out.println("First Name:");
		String fName = customerInput.nextLine();
		System.out.println("Last Name:");
		String lName = customerInput.nextLine();
		System.out.println("SSN:");
		String ssn = customerInput.nextLine();
		System.out.println("Address:");
		String address = customerInput.nextLine();
		System.out.println("DOB:");
		String dob = customerInput.nextLine();

		Customer customer = new Customer(fName, lName, ssn, address, dob);

		System.out.println("Customer  details: " + customer);
		bankingInterface.addCustomer(customer);
		bankingInterface.printBankStatus();
	}

	private void addCustomerAccount() {
		Customer customer = retrieveCustomerInformation();
		if (customer != null) {
			AccountType accountType = getCustomerAccountType();
			Double openingBalance = getOpeningBalnce();
			Account account = new Account(accountType, new Date(), openingBalance);
			customer.addAccount(account);
		} else
			System.out.println("Customer does not available!!!");

	}

	private Customer retrieveCustomerInformation() {
		Scanner input = getScanner();
		System.out.println("enter SSN:");
		String ssn = input.nextLine();
		return bankingInterface.getCustomerInfo(ssn);
	}

	private AccountType getCustomerAccountType() {
		System.out.println("Select the customer account type:");
		System.out.println("1 for CHECKING");
		System.out.println("2 for SAVING");
		System.out.println("3 for BUSINESS_CHECKING");

		AccountType accountType = null;
		switch (getScanner().nextInt()) {
		case 1:
			accountType = AccountType.CHECKING;
			break;
		case 2:
			accountType = AccountType.SAVING;
			break;
		case 3:
			accountType = AccountType.BUSINESS_CHECKING;
			break;
		default:
			accountType = AccountType.CHECKING;
		}

		return accountType;
	}

	private Double getOpeningBalnce() {
		System.out.println("Opening balance:");
		return getScanner().nextDouble();
	}

	private void printBankStatus() {
		// TODO Auto-generated method stub

	}

	private void sendMoney() {
		// TODO Auto-generated method stub

	}

	private static Scanner getScanner() {
		return new Scanner(System.in);
	}

	public BankingInterface getBankingInterface() {
		return bankingInterface;
	}

	public void setBankingInterface(BankingInterface bankingInterface) {
		this.bankingInterface = bankingInterface;
	}
}
