package com.assignment.bank.enumConcept;

public enum DeliveryOption {
	TEN_MINUTES, TWENTY_FOUR_HOURS
}
