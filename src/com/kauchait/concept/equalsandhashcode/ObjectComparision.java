package com.kauchait.concept.equalsandhashcode;

public class ObjectComparision {
	public static void main(String[] args) {
		Employee e1 = new Employee("David", "Main st", "123456789", "123");

		Employee e2 = new Employee("David", "Main st", "123456789", "123");
		System.out.println(e1.hashCode());
		System.out.println(e2.hashCode());

		if (e1.equals(e2)) {
			System.out.println("two objects are equal");
		}

		/*
		 * if (e1.getAddress() != null) { 
		 * if (e1.getAddress().equals(e2.getAddress())) {
		 * System.out.println("I am here"); 
		 * } 
		 * }
		 */
	}

}
