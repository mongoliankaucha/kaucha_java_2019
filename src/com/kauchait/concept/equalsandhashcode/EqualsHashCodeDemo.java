package com.kauchait.concept.equalsandhashcode;

import java.util.Date;

public class EqualsHashCodeDemo {
	public static void main(String[] args) {

		// In order to compare two Java Objects, its
		// good practice to override Hashcode and Equals method of Object
		Employee e1 = new Employee("David Smith", "121 Main St", "222111333", "101");
		e1.sDate = new Date();
		Employee e2 = new Employee("David Smith", "121 St", "222111333", "101");

		System.out.println(e1.hashCode());
		System.out.println(e2.hashCode());

		System.out.println(e1.equals(e2));

		Employee e3 = new Employee();
		Employee e4 = new Employee();

		if (e3.equals(e4)) {
			System.out.println("we are equal");
		} else {
			System.out.println("we are not equal");
		}
		// System.out.println(e==e2);
	}

}
