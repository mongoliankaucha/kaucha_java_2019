package com.kauchait.concept.equalsandhashcode;

import java.io.Serializable;
import java.util.Date;

public class Employee implements Serializable {
	private String name;
	private String address;
	private String ssn;
	private String number;
	protected Date sDate;

	public Employee() {

	}

	public Employee(String name, String ssn) {
		super();
		this.name = name;
		this.ssn = ssn;
	}

	public Employee(String name, String address, String ssn, String number) {
		super();
		this.name = name;
		this.address = address;
		this.ssn = ssn;
		this.number = number;
	}

	public void studentInfo() {
		System.out.println("Student Name:  " + name + " " + address);
	}

	public String getName() {
		return name;
	}

	public String getAddress() {
		return address;
	}

	public String getSsn() {
		return ssn;
	}

	public String getNumber() {
		return number;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((ssn == null) ? 0 : ssn.hashCode());
		return result;
	}

	//if name and ssn are same of two object, it will produces same hash code
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (ssn == null) {
			if (other.ssn != null)
				return false;
		} else if (!ssn.equals(other.ssn))
			return false;
		return true;
	}

	public void getdisplay() {
		System.out.println(toString());
	}

	@Override
	public String toString() {
		return "Employee [name=" + name + ", address=" + address + ", ssn=" + ssn + ", number=" + number + ", sDate="
				+ sDate + "]";
	}

}
