package com.kauchait.concept.equalsandhashcode;

import java.util.ArrayList;

public class EmployeeTest {
	private static ArrayList<Employee> empList = new ArrayList<>();

	public static void main(String[] args) {

		Employee emp1 = new Employee("hari", "Dallas", "123", "789");
		Employee emp2 = new Employee("hari", "Irving", "123", "456");

		if (emp1.equals(emp2)) {
			System.out.println("we are equal");
		} else {
			System.out.println("we are unequal");
		}

		addEmployee(emp1);
		addEmployee(emp2);

		System.out.println(empList.size());
		
		emp1.getdisplay();
		emp2.getdisplay();
	}

	private static void addEmployee(Employee emp) {
		// TODO Auto-generated method stub
		if (!empList.contains(emp)) {
			empList.add(emp);
		}
	}

}
