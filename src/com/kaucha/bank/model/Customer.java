package com.kaucha.bank.model;

import java.util.ArrayList;

public class Customer {

	private String customerName;
	private String address;
	private ArrayList<Account> accounts;

	Customer() {
		this.accounts = new ArrayList<Account>();
	}

	public Customer(String customerName, String address, ArrayList<Account> accounts) {
		this.customerName = customerName;
		this.address = address;
		this.accounts = accounts;
	}

	public Customer(String customerName, String address) {
		this.customerName = customerName;
		this.address = address;
		this.accounts= new ArrayList<Account>();
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	// Accessor
	public String getAddress() {
		return address;
	}

	// Modifier
	public void setAddress(String address) {
		this.address = address;
	}

	public ArrayList<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(ArrayList<Account> accounts) {
		this.accounts = accounts;
	}

	public void printCustomerAccounts() {
		// TODO print customerAccount
		System.out.println(getAccounts());
	}

	public void printcustomerDetails() {
		// TODO print customerDetails
		System.out.println(toString());

	}

	@Override
	public String toString() {
		return "Customer [customerName=" + customerName + ", address=" + address + ", accounts=" + accounts + "]";
	}

}
