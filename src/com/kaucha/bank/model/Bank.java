package com.kaucha.bank.model;

import java.util.ArrayList;

public class Bank {
	private String bankName = "Bank of Kaucha";
	private ArrayList<Customer> customers;
	
	Bank(){
		this.customers = new ArrayList<Customer>();
	}

	public ArrayList<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(ArrayList<Customer> customers) {
		this.customers = customers;
	}

	public String getBankName() {
		return bankName;
	}

	public void printBankName() {
		// TODO print bankName
		System.out.println(getBankName());
	}

	public void printBankDetails() {
		// TODO print bankDetails
		System.out.println(toString());
	}
	
	public void addCustomer(Customer customer) {
		this.customers.add(customer);
	}

	@Override
	public String toString() {
		return "Bank [bankName=" + bankName + ", customers=" + customers + "]";
	}

}
