package com.kaucha.bank.model;

import java.util.ArrayList;
import java.util.Scanner;

public class BankAdmin {// Class should have single responsibility
	public static void main(String[] args) {
		System.out.println("Number of customer:");
		int numberOfCustomerToBeAdded = getScanner().nextInt();

		Bank bank = new Bank();
		for (int i = 0; i < numberOfCustomerToBeAdded; i++) {
			addCustomertoBank(bank);

			bank.printBankName();
			bank.printBankDetails();
			newLine();
		}
	}

	private static void addCustomertoBank(Bank bank) {
		System.out.println("Customer name:");
		String name = getScanner().nextLine();
		System.out.println("Address:");
		String address = getScanner().nextLine();
		System.out.println("Account Type:");
		String accountType = getScanner().nextLine();

		Account customerAccount = new Account(accountType);

		ArrayList<Account> accountsList = new ArrayList<>();
		accountsList.add(customerAccount);

		Customer customer = new Customer(name, address, accountsList);

		bank.addCustomer(customer);
	}

	private static Scanner getScanner() {
		return new Scanner(System.in);
	}

	private static void newLine() {
		// TODO Auto-generated method stub
		System.out.println(
				"...................................................................................................................................");

	}

}
