package com.cerotid.oo.principles;

public abstract class PetAnimal extends Animal {

	private boolean isInsured;

	public void habitate() {
		System.out.println("I live with people in their house");
	}

	// overriding
	public void eat() {
		System.out.println("I eat expensive food");
	}

	public boolean isInsured() {
		return isInsured;
	}

	public void setInsured(boolean isInsured) {
		this.isInsured = isInsured;
	}

}
