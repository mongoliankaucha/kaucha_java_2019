package com.cerotid.oo.principles;

public class Cat extends PetAnimal {
	@Override
	public void habitate() {
		System.out.println("Wherever I feel like");
	}

	@Override
	public void sleeptime() {
		System.out.println("Whatever!");
	}
}
