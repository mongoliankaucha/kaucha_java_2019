package com.cerotid.oo.principles;

import com.cerotid.oo.animal.Veterinary;

public class AnimalTest {

	public static void main(String[] args) {
		// create Animal object
		System.out.println("I am first Animal");
		Animal animal1 = new Zebra();
		animal1.breathe();
		animal1.eat();
		animal1.makeSound();
		System.out.println();

		// create WildAnimal object
		System.out.println("I am first Wild Animal");
		WildAnimal wildAnimal = new Zebra();
		wildAnimal.breathe();
		wildAnimal.eat();
		wildAnimal.makeSound();
		wildAnimal.habitate();

		System.out.println();
		System.out.println("I am a lion");
		Lion lion = new Lion();
		lion.eat();

		// Animal=Dog which is also an Animal
		Animal animal2 = new Dog();
		//Animal animal3 = WildAnimal1;

		Dog dog = new Dog(true, "white");

		System.out.println("I am insured: " + dog.isInsured() + " & I have this many teeth: " + dog.getNumberOfTeeth());
		// (Dog!=Animal, because Animal is not necessarily a dog)
		// Dog dog=new Animal();

		Animal cowAnimal = new Cow();
		cowAnimal.eat();

		Cow cow = new Cow();
		cow.eat();

		// upcasting
		Animal aCow = cow;
		Object a = aCow;

		Veterinary veterinary = new Veterinary();
		System.out.println();
		veterinary.treatAnimal(dog);
		veterinary.treatAnimal(cow);
		veterinary.treatAnimal(lion);
		veterinary.treatAnimal(wildAnimal);
		
		
		veterinary.displayVendingMachine();
		veterinary.groomAnimal(dog);
	}

}
