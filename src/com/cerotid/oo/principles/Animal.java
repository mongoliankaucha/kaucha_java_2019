package com.cerotid.oo.principles;

public abstract class Animal {
	private String color;
	private final int limbs = 4;
	private int numberOfTeeth;
	private boolean isSick;
	
	public abstract void sleeptime();

	public final void breathe() {
		System.out.println("Breathing in O2, breathing out CO2");
	}

	public void eat() {
		System.out.println("Eating food");
	}

	public void makeSound() {
		System.out.println("I don't know which sound to produce");
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getNumberOfTeeth() {
		return numberOfTeeth;
	}

	public boolean isSick() {
		return isSick;
	}

	public void setSick(boolean isSick) {
		this.isSick = isSick;
	}

	public void setNumberOfTeeth(int numberOfTeeth) {
		this.numberOfTeeth = numberOfTeeth;
	}

	public int getLimbs() {
		return limbs;
	}

	@Override
	public String toString() {
		return "Animal [color=" + color + ", limbs=" + limbs + ", numberOfTeeth=" + numberOfTeeth + "]";
	}

}
