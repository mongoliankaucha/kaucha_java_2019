package com.cerotid.oo.principles;

public class Zebra extends WildAnimal {
	@Override
	public void eat() {
		System.out.println("Even though I am wild, I just eat grains and leaves");
	}

	@Override
	public void habitate() {
		System.out.println("I live in jungle, but scared of lion");
	}

	@Override
	public void sleeptime() {
		System.out.println("When I feel like I am protected");
		
	}

}
