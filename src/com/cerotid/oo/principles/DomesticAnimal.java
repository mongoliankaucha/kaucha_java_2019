package com.cerotid.oo.principles;

public class DomesticAnimal extends Animal {
	public void habitate() {
		System.out.println("I live arround the people");
	}

	// overriding
	public void eat() {
		System.out.println("I eat whatever people allow me to eat");
	}

	@Override
	public void sleeptime() {
		System.out.println("Whenever I am not eating!");
	}
	

}
