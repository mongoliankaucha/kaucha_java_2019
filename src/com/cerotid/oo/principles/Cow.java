package com.cerotid.oo.principles;

public class Cow extends DomesticAnimal {
	@Override
	public void eat() {
		System.out.println("I am different from my parent. I only eat Green grass!");
	}
}
