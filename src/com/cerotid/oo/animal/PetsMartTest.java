package com.cerotid.oo.animal;

import com.cerotid.oo.implementsConcept.CokeVendingMachine;
import com.cerotid.oo.implementsConcept.Hotel;
import com.cerotid.oo.implementsConcept.MuscleMilkVendingMachine;
import com.cerotid.oo.implementsConcept.SnacksVendingMachine;
import com.cerotid.oo.interfaceConcept.VendingMachine;

public class PetsMartTest {
	public static void main(String[] args) {
		VendingMachine cokeVendingMachine = new CokeVendingMachine();
		Petsmart petsmart = new Petsmart(cokeVendingMachine);
		petsmart.getVendingMachineFranchiser().displayVendingMachine();

		VendingMachine muscleMilkVendingMachine = new MuscleMilkVendingMachine();
		petsmart.setVendingMachineFranchiser(muscleMilkVendingMachine);
		petsmart.getVendingMachineFranchiser().displayVendingMachine();

		VendingMachine snacksVendingMachine = new SnacksVendingMachine();
		petsmart.setVendingMachineFranchiser(snacksVendingMachine);
		petsmart.getVendingMachineFranchiser().displayVendingMachine();

		VendingMachine hotel = new Hotel();
		petsmart.setVendingMachineFranchiser(hotel);
		petsmart.getVendingMachineFranchiser().displayVendingMachine();

	}
}
