package com.cerotid.colletions;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Scanner;
import java.util.TreeMap;

public class MapConcept {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println("HashMap");
		HashMap<Integer, String> hashMap = new HashMap<Integer, String>();

		System.out.println(hashMap.isEmpty());

		hashMap.put(2, "puranogau, wari");
		hashMap.put(3, "ghorghore");
		hashMap.put(1, "puranogau, pari");
		hashMap.put(4, "ranasing, mathi");
		hashMap.put(5, "ranasing, tala");
		hashMap.put(9, "farsa, tala");
		hashMap.put(6, "farsa, mathi");
		hashMap.put(7, "kholakharka+pokharadada");
		hashMap.put(8, "bhedikhor");
		hashMap.put(5, "ranasing, tala");
		hashMap.put(null, "some name");
		hashMap.put(null, "another name");

		System.out.println(hashMap);
		System.out.println(hashMap.get(2));
		System.out.println(hashMap.isEmpty());
		System.out.println(hashMap.keySet());

		System.out.println("LinkedHashMap");

		LinkedHashMap<Integer, String> linkedHashMap = new LinkedHashMap<Integer, String>();

		System.out.println(hashMap.isEmpty());

		linkedHashMap.put(2, "puranogau, wari");
		linkedHashMap.put(3, "ghorghore");
		linkedHashMap.put(1, "puranogau, pari");
		linkedHashMap.put(4, "ranasing, mathi");
		linkedHashMap.put(5, "ranasing, tala");
		linkedHashMap.put(9, "farsa, tala");
		linkedHashMap.put(6, "farsa, mathi");
		linkedHashMap.put(7, "kholakharka+pokharadada");
		linkedHashMap.put(8, "bhedikhor");
		linkedHashMap.put(5, "ranasing, tala");

		System.out.println(linkedHashMap);
		System.out.println(linkedHashMap.get(2));
		System.out.println(linkedHashMap.isEmpty());
		System.out.println(linkedHashMap.keySet());

		System.out.println("TreeMap");

		TreeMap<Integer, String> treeMap = new TreeMap<Integer, String>();

		System.out.println(treeMap.isEmpty());

		treeMap.put(2, "puranogau, wari");
		treeMap.put(3, "ghorghore");
		treeMap.put(1, "puranogau, pari");
		treeMap.put(4, "ranasing, mathi");
		treeMap.put(5, "ranasing, tala");
		treeMap.put(9, "farsa, tala");
		treeMap.put(6, "farsa, mathi");
		treeMap.put(7, "kholakharka+pokharadada");
		treeMap.put(8, "bhedikhor");
		treeMap.put(5, "ranasing, tala");

		System.out.println(treeMap);
		System.out.println(treeMap.get(2));
		System.out.println(treeMap.isEmpty());
		System.out.println(treeMap.keySet());

		while (true) {
			try {
				System.out.println("enter ward number of ranasing-kitenee vdc:");
				int n = sc.nextInt();
				System.out.println("village: " + hashMap.get(n));
				newLine();
			} catch (Exception e) {
				System.out.println("error");
				break;
			}
			// break;
		}
	}

	private static void newLine() {
		System.out.println();
	}

}
