package com.cerotid.colletions;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.TreeSet;

public class SetConcept {
	public static void main(String[] args) {

		System.out.println("HashSet:");
		HashSet<String> hashSet = new HashSet<String>();

		hashSet.add("Rakesh");
		hashSet.add("Rakesh");
		hashSet.add("David");
		hashSet.add("Sunil");
		hashSet.add("Sanjiv");
		// Can add null element as well
		hashSet.add(null);
		hashSet.add(null);

		System.out.println(hashSet);
		// hashSet can be change into TreeSet while removing null
		hashSet.remove(null);

		TreeSet<String> tset = new TreeSet<>(hashSet);
		System.out.println(tset);

		System.out.println("LinkedHashSet:");
		LinkedHashSet<String> linkedHashSet = new LinkedHashSet<String>();

		linkedHashSet.add("Rakesh");
		linkedHashSet.add("Rakesh");
		linkedHashSet.add("David");
		linkedHashSet.add("Sunil");
		linkedHashSet.add("Sanjiv");

		// Can add null element as well set.add(null);
		linkedHashSet.add(null);

		System.out.println(linkedHashSet);

		// Converting LinkedHashSet to TreeSet
		linkedHashSet.remove(null);
		TreeSet<String> trset = new TreeSet<>(linkedHashSet);
		System.out.println(trset);

		System.out.println("TreeSet:");
		TreeSet<String> treeSet = new TreeSet<String>();

		treeSet.add("Rakesh");
		treeSet.add("Rakesh");
		treeSet.add("David");
		treeSet.add("Sunil");
		treeSet.add("Sanjiv");

		System.out.println(treeSet);
		Iterator it = treeSet.iterator();

		while (it.hasNext()) {
			String obj = (String) it.next();
			System.out.println(obj);
		}

	}
}
