package com.cerotid.general.assignment;

import java.util.Scanner;

public class PrimeNumberTester {

	public static void main(String[] args) {

		while (true) {
			displayMenu();
			actionMenuPerform();
		}

	}

	private static void actionMenuPerform() {
		// TODO Auto-generated method stub

		int upperLimit = 0;
		int lowerLimit = 0;

		while (true) {
			try {
				switch (getScanner().nextInt()) {
				case 1:

					PrimeNumberFinder1 pn1 = new PrimeNumberFinder1();
					pn1.FindPrimeNumber1(getLowerLimit(lowerLimit), getUpperLimit(upperLimit));
					break;

				case 2:

					PrimeNumberFinder2 pn2 = new PrimeNumberFinder2();
					pn2.FindPrimeNumber2(getLowerLimit(lowerLimit), getUpperLimit(upperLimit));
					break;

				case 3:
					PrimeNumberFinder3 pn3 = new PrimeNumberFinder3();
					pn3.FindPrimeNumber3(getLowerLimit(lowerLimit), getUpperLimit(upperLimit));
					break;

				case 4:
					PrimeNumberFinder4 pn4 = new PrimeNumberFinder4();
					pn4.FindPrimeNumber4(getLowerLimit(lowerLimit), getUpperLimit(upperLimit));
					break;

				case 0:
					System.out.println("Exit");
					System.exit(0);
					break;
				default:
					break;
				}
				break;
			} catch (Exception e) {
				System.out.println("error");

			}
		}

	}

	private static void displayMenu() {
		// TODO Auto-generated method stub
		System.out.println();
		System.out.println("Prime number finder program:");
		System.out.println("enter 1 to use for-for loop");
		System.out.println("enter 2 to use while-while loop");
		System.out.println("enter 3 to use while-for loop");
		System.out.println("enter 4 to use for-while loop");
		System.out.println("enter 0 to exit");

	}

	private static int getUpperLimit(int upperLimit) {
		System.out.println("enter upper limit");
		return getScanner().nextInt();
	}

	private static int getLowerLimit(int lowerLimit) {
		System.out.println("enter lower limit");
		return getScanner().nextInt();
	}

	private static Scanner getScanner() {
		return new Scanner(System.in);
	}

}
