package com.cerotid.general.assignment;

import java.util.Scanner;

public class EmployeeTest {

	public static void main(String[] args) {

		Employee employee1 = new Employee();
		employee1.name = "himal";
		employee1.ssn = "123456789";
		employee1.salary = 5000;
		employee1.printEmployeeNameAndSSN();
		employee1.printEmployeeNameAndSalary();
		System.out.println();

		Employee employee2 = new Employee();
		employee2.name = "prakash";
		employee2.ssn = "987654321";
		employee2.salary = 2000;
		print(employee2);
		newline();

		Employee employee3 = new Employee("Michael", "123456789", 222333.0);
		employee3.printEmployeeNameAndSSN();
		employee3.printEmployeeNameAndSalary();
		newline();

		Employee employee = new Employee(getName(), getSSN(), getSalary());
		print(employee);
		newline();

	}

	private static void print(Employee employee) {
		employee.printEmployeeNameAndSSN();
		employee.printEmployeeNameAndSalary();
	}

	private static double getSalary() {
		System.out.println("enter salary:");
		return getScanner().nextDouble();
	}

	private static String getSSN() {
		System.out.println("enter ssn:");
		return getScanner().nextLine();

	}

	private static String getName() {
		System.out.println("enter name:");
		return getScanner().nextLine();

	}

	private static Scanner getScanner() {
		return new Scanner(System.in);
	}

	private static void newline() {
		System.out.println();
	}

}
