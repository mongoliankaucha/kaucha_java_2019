package com.cerotid.general.assignment;

public class PrimeNumberFinder3 {

	public void FindPrimeNumber3(int lowerLimit, int upperLimit) {
		System.out.println("Prime numbers between " + lowerLimit + " and " + upperLimit + " are:");
		for (int i = lowerLimit; i <= upperLimit; i++) {
			int count = 0;
			int j = 1;
			while (j <= i) {
				if (i % j == 0) {
					count = count + 1;
				}
				j++;
			}
			if (count == 2) {
				System.out.print(i + " ");
			}
		}
	}
}
