package com.cerotid.general.assignment;

public class PrimeNumberFinder4 {

	public void FindPrimeNumber4(int lowerLimit, int upperLimit) {
		System.out.println("Prime numbers between " + lowerLimit + " and " + upperLimit + " are:");
		int i = lowerLimit;

		while (i <= upperLimit) {
			int count = 0;
			for (int j = 1; j <= i; j++) {
				if (i % j == 0) {
					count = count + 1;
				}

			}
			if (count == 2) {
				System.out.print(i + " ");
			}
			i++;

		}
	}
}