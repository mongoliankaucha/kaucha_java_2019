package com.cerotid.general.assignment;

public class PrimeNumberFinder1 {
	int sum = 1;
	void FindPrimeNumber1(int lowerLimit, int upperLimit) {
		System.out.println("Prime numbers between " + lowerLimit + " and " + upperLimit + " are:");
		
		for (int i = lowerLimit; i <= upperLimit; i++) {
			int count = 0;
			for (int j = 1; j <= i; j++) {

				if (i % j == 0) {
					count = count + 1;
				}

			}
			printPrimeNumber(count, i);
			

		}
	}

	private void printPrimeNumber(int count, int i) {
		// TODO Auto-generated method stub
		
		if (count == 2) {
			if (sum%10!=0) {
				System.out.print(i + " ");
				sum = sum + 1;
			} else {
				System.out.println(i + " ");
				sum = sum + 1;
			}

		}
		
	}
}
