package com.cerotid.general.assignment;

public class Employee {
	// state, member variable name, ssn, salary
	String name;
	String ssn;
	double salary;

	public Employee() {
		// TODO Auto-generated constructor stub
	}

	public Employee(String name, String ssn, double salary) {
		if(ssn.length()==9)
		this.name = name;
		this.ssn = ssn;
		this.salary = salary;
		
	}

	void printEmployeeNameAndSSN() {

		printName();
		System.out.println("Employee SSN: " + ssn);

	}

	private void printName() {
		// TODO Auto-generated method stub
		System.out.println("Name: " + name);
	}

	void printEmployeeNameAndSalary() {
		printName();
		System.out.println("Salary:" + salary);
	}
}
