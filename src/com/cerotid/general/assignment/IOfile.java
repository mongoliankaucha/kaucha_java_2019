package com.cerotid.general.assignment;

import java.io.File;
import java.util.Formatter;
import java.util.Scanner;

public class IOfile {
	
	public static void main(String[] args) {
		
		try {
			String name="himal kaucha";
			Formatter f=new Formatter("Himal.txt");
			f.format("%s", name);
			System.out.println("file saved");
			f.close();
		} catch (Exception e) {
			System.out.println("error");
		}

		
		try {
			File file=new File("Himal.txt");
			Scanner sc=new Scanner(file);
			while(sc.hasNext())
			System.out.println(sc.next());
		} catch (Exception e) {
			System.out.println("error");
			e.printStackTrace();
		}
		
	}

}
