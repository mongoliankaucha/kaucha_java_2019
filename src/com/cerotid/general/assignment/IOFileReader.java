package com.cerotid.general.assignment;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import javafx.collections.transformation.SortedList;

public class IOFileReader {
	public static void main(String[] args) {
		try {
			  File x = new File("Customers.txt");
			  Scanner sc = new Scanner(x);
			  while(sc.hasNext()) {
			    System.out.println(sc.next());
			  }
			  sc.close();
			} catch (FileNotFoundException e) {
			  System.out.println("Error");
			}
	}

}
