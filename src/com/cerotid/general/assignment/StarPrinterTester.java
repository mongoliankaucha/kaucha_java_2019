package com.cerotid.general.assignment;

import java.util.Scanner;

public class StarPrinterTester {
	public static void main(String[] args) {
		StarPrinter sp = new StarPrinter();
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter any number:");
		
		int n = sc.nextInt();
		sp.n = n;
		sp.formate();
		
		sc.close();
	}
}
