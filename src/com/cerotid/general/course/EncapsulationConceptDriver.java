package com.cerotid.general.course;

public class EncapsulationConceptDriver {
	private static String NAME="Michael";
	private static String SSN="123456789";
	private static final String STATE_CODE="OH";
	
public static void main(String[] args) {
	EncapsulationConcept enc=new EncapsulationConcept();
	
	//enc.name="Himal";
	//enc.ssn="123-456-789";
	
	enc.setName(NAME);
	enc.setSsn(SSN);
	
	enc.printEmoloyeeInfo();
}
}
