package com.cerotid.general.course;

public class Calculator {
	//class level variables - hold state
	int result;
	
	
	/*methods are behaviors
	 * sum
	 * sub
	 * mul
	 * div
	 * sqrt
	 */
	int sum(int a, int b) {
		return a+b;
	}
	int sum(int a, int b, int c) {
		return a+b+c;
		
	}
	
	void mul(int a, int b) {
		//local variables
		//int random=9;
		result= a*b;  //*random;
	}
	
	
	
	public static void main(String[] args) {
		/*
		 * instantiation
		 * create calculator object (ie new Calculator()) and assign to a variable of type Calculator
		 */
	
		Calculator myFirstObject=new Calculator();
		int sum=myFirstObject.sum(100, 200);
		System.out.println("Sum:"+sum);
		sum=myFirstObject.sum(100, 200, 300);
		System.out.println("Sum:"+sum);
		myFirstObject.mul(2, 10);
		System.out.println("Mul:"+myFirstObject.result);
		
		Calculator mySecondObject =new Calculator();
		
		Calculator myThirdObject =new Calculator();
		
		
		
	}
}
