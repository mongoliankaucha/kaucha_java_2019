package com.cerotid.general.course;

import java.util.Scanner;

public class RecursionConceptTest {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		while(true) {
		try {
		System.out.println("Enter a number to find factorial: ");
		int n = sc.nextInt();
		

		RecursionConcept rc = new RecursionConcept();
		int factorial = rc.factorial(n);

		System.out.println("Factorial of " + n + " is: " + factorial);
		System.out.println("Factorial: " + rc.factorial1(n));
		break;
		}catch(Exception e) {
			System.out.println("error!");
			break;
		}
		}
		
		}

}
