package com.cerotid.general.course;

public class HelloWorld {

	public static void main(String[] args) {
		
		System.out.println("Hello World");
		System.out.println("thanks for your support !!!");
	
		
		//variable
		//something to assign on left, value on right
		//primitives
		
		short a=2;
		int x=5;
		long ln=22222222;
		double y=5.5;
		float dn=454.34f;
		char c='d';
		
		//operations
		x=x+5;
		System.out.println("variable of x="+x);
		
		x++; //x=x+1;
		System.out.println("variable of x="+x);
		
		x--; //x=x-1;
		System.out.println("variable of x="+x);
		
		//conditions 
		//control flow
		//if <Statement True> [Do this] Else if <Statement True> [Do that] For Everything else {do something else}

		int age=30;
		if(age<5) {
			System.out.println("Send to Day Care");
		}
		else if(age>=5&&age<18) {
			System.out.println("Go to school");
		}
		else if (age>=18&&age<24){
			System.out.println("Go to college");
		}
		else {
			System.out.println("You do not have life!~");
		}
		
	}
	
	
}
