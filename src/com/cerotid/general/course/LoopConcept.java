package com.cerotid.general.course;

public class LoopConcept {
	
	public static void main(String[] args) {
		/*
		 * Types of loop:
		 * for loop
		 * while loop
		 * do while loop
		 * foreach 
		 */
		//for loop
		for(int i=0; i<5; i++) {
			System.out.println("My name is himal");
		}
		
		//while loop
		int j=0;
		while(j<5) {
			System.out.println("I am in java class");
			j++; //j=j+1;
			System.out.println("value of j="+j);
		}
		
		//do-while
		int k=0;
		do {
			System.out.println("I am in a path to be a programmer");
			k++; //k=k+1;
		}while(k<5);
		
		//
	}

}
