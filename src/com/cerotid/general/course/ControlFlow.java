package com.cerotid.general.course;

import java.util.Scanner;

public class ControlFlow {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		
		//Example of If-Else if-Else
		System.out.println("Enter your age:");
		int age=sc.nextInt();
		
		if(age<5) {
			System.out.println("Send to Day Care");
		}
		else if(age>=5&&age<18) {
			System.out.println("Go to school");
		}
		else if (age>=18&&age<24){
			System.out.println("Go to college");
		}
		else {
			System.out.println("You do not have life!~");
		}
		
		//if
		if(age>=65) {
			System.out.println("You can now get social security benefit !");
		}
		
		//Expresion
		//?then
		//else:
		String seniority =(age>=65) ? "Senior":"Not yet senior";
			
		//Equivalent to above
		if(age>=65) {
			seniority="senior";
		}
		else
			seniority="Not yet senior";
		
		//Siwtch case
		String sports= "Cricket";
		
		switch(sports) {
		case "Football":
			System.out.println("I love football");
			break;
		case "Baseball":
			System.out.println("I love baseball");
			break;
		case "Cricket":
			System.out.println("I love cricket");
			break;
		default:
			System.out.println("Hey, I don't like any sports");
		break;
		}
		
	}

}
