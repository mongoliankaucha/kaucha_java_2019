package com.cerotid.general.course;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ScannerInputConcept {
	static Scanner input;

	public static void main(String[] args) {

		int i = 0;
		do {
			getScannerInput();
			// Scanner input = new Scanner(System.in);
			System.out.println("Enter your name: ");
			String userName = input.nextLine();
			System.out.println("Age: ");
			int age = 0;
			while (true) {
				try {
					getScannerInput();
					age = input.nextInt();
					break; // breaks the immediate loop
				} catch (InputMismatchException e) {
					System.out.println("Error!");
				}
			}
			newLine();
			printUserName(userName);
			printUserAge(age);
			newLine();

			i++;

		} while (i < 5);
	}

	private static void getScannerInput() {
		input = new Scanner(System.in);

	}

	private static void printUserName(String userName) {

		System.out.println("Name: " + userName);

	}

	private static void printUserAge(int age) {
		if(age>100&&age<129) {
			System.out.println("Age: " + age);
			System.out.println("may not be fair!");
		}
		else if(age>130) {
			System.out.println("Age: " + age);
			System.out.println("Impossible!");
		}
		else {
			System.out.println("Age: " + age);
		}
		
	}

	private static void newLine() {
		System.out.println();
	}
}
