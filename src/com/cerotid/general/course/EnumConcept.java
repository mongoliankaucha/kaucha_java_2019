package com.cerotid.general.course;

public enum EnumConcept {
	
	SUMMER ("summer"), SPRING ("spring"), FALL ("fall"), WINTER ("winter");

	private final String weather;
	EnumConcept(String a){
		this.weather=a;
	}
	public String getWeather() {
		return weather;
	}
	
}
