package com.cerotid.general.course;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class ArrayListSort {

	public static void main(String[] args) {
		ArrayList<String> collect = new ArrayList<String>();
		collect.add("himal kaucha");
		collect.add("chandra kumar");
		collect.add("sunil tondon");
		collect.add("dhanjit tamang");
		System.out.println("Collections: " + collect);
		Collections.sort(collect);
		System.out.println("Sorted list: " + collect);
		System.out.println();

		ArrayList<Integer> number = new ArrayList<Integer>();
		number.add(1);
		number.add(3);
		number.add(5);
		number.add(2);
		number.add(4);

		System.out.println("Numbers: " + number);

		Collections.reverse(number);
		System.out.println("Reversed list: " + number);

		Collections.sort(number);
		System.out.println("Sorted list: " + number);
		
		Collections.reverse(number);
		System.out.println("Reversed list: " + number);

		Collections.shuffle(number);
		System.out.println("Shuffled (random) list: " + number);

		int x = Collections.max(number);
		System.out.println("Maximum number: " + x);

		int y = Collections.min(number);
		System.out.println("Minimum number: " + y);
	}

}
