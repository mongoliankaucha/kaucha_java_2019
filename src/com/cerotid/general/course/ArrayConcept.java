package com.cerotid.general.course;

public class ArrayConcept {

	public static void main(String[] args) {
		// Array Concept

		int[] arrayInteger = new int[10];

		arrayInteger[0] = 100;
		arrayInteger[5] = 500;
		arrayInteger[8] = 200;
		arrayInteger[1] = 50;

		arrayInteger[9] = 1000;

		for (int i = 0; i < 10; i++) {
			System.out.println("Value in array position " + i + ": " + arrayInteger[i]);
		}
		System.out.println("Size of array:" + arrayInteger.length);
		
		System.out.println("Sum :" + (arrayInteger[0] + arrayInteger[5]));
	}
}
