package com.cerotid.general.course;

class Outer { 
    void outerMethod() { 
        System.out.println("I am from Outer class"); 
        // Inner class is local to outerMethod() 
        class Inner { 
            void innerMethod() { 
                System.out.println("I am from Inner class"); 
            } 
        } 
        Inner inner = new Inner(); 
        inner.innerMethod(); 
    } 
} 
