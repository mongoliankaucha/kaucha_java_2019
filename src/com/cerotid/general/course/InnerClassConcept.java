package com.cerotid.general.course;

public class InnerClassConcept {
	public static void main(String[] args) {
		InnerClassConcept inc=new InnerClassConcept();
		
		//instantiate inner class A
		//InnerA innerClassA=inc.new InnerA();
		
		//instantiate inner class B
		InnerB innnerClassBObject=inc.new InnerB();
		innnerClassBObject.accessInnerPropertyOfB();
		
		InnerA.accessInnerProperty();
	}
	
	private static class InnerA{

		public static void accessInnerProperty() {
			// TODO Auto-generated method stub
			
			
		}
		
	}
	private class InnerB{

		public void accessInnerPropertyOfB() {
			// TODO Auto-generated method stub
			
			
		}
		
	}

}
