package com.cerotid.general.course;

import java.util.ArrayList;
import java.util.Collection;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils.Collections;

public class ArrayListConcept {
	public static void main(String[] args) {
		ArrayList aList=new ArrayList();
		
		aList.add("cat");
		aList.add("dog");
		aList.add(new Integer(2));
		aList.add(1);
		aList.add(1000);
		aList.add("ball");
		
		System.out.println(aList);
		newLine();
		
		arrayListPrinter(aList);
		newLine();
		
		System.out.println(aList.get(4));
		newLine();
		
		aList.remove("dog");
		aList.remove(4);
		
		System.out.println(aList);
		newLine();
		
		arrayListPrinter(aList);
		
		
		ArrayList bList=new ArrayList();
		
		bList.add("goat");
		bList.add("fish");
		bList.add(new Integer(2500));
		bList.add(1.0);
		bList.add("helmet");
		bList.add(10000);
		
		newLine();
		aList.addAll(bList);
		arrayListPrinter(aList);
		
		//System.out.println("Your String= "+a);
		//System.out.println("Your integer= "+i);
		//System.out.println(aList);
	}
	
	static void newLine() {
		System.out.println();
	}
	
	
	static void arrayListPrinter(ArrayList aList) {

		Integer i=0;
		String a=null;
		
		for (Object object : aList) {
			if (object instanceof String) {
				a= (String) object;
				System.out.println("Your String= "+a);
			}
			else if(object instanceof Integer) {
				i=(Integer) object;
				System.out.println("Your integer= "+i);
			}
			
		}
		
	}
}


