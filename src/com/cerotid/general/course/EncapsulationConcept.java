package com.cerotid.general.course;

//hiding the information
public class EncapsulationConcept {
	//String name;
	//String ssn;
	
	private String name;
	private String ssn;
	
	void printEmoloyeeInfo(){
		System.out.println("Name:"+name+"\n"+"SSN:"+ssn);
	}
	
	//setter-mutator
	public void setName(String x) {
		name=x;
	}

	public void setSsn(String ssn) {
		if(ssn.length()!=9) {
			this.ssn=null;
			return;
	
		}
		else {
			this.ssn = ssn;
		}
		
	}

}
