package com.cerotid.general.course;

public class EnumConceptTest {
	public static void main(String[] args) {
		
		System.out.println(EnumConcept.SPRING);
		
		EnumConcept enumConcept=EnumConcept.FALL;
		
		switch (enumConcept){
		case SUMMER:
			System.out.println("I am SUMMER");
			break;
		case SPRING:
			System.out.println("I am SPRING");
			break;
		case WINTER:
			System.out.println("I am WINTER");
			break;
		case FALL:
			System.out.println("I am FALL");
			break;
		default:
			System.out.println("UNKNOWN");
			break;
		}
		
		System.out.println(EnumConcept.SUMMER.getWeather());
	}

}
