package com.cerotid.general.course;

public class RecursionConcept {

	// Best recursion process
	public int factorial(int num) {
		if (num == 0) {
			return 1;
		} else {
			return num * factorial(num - 1);
		}
	}

	// Just for value
	int fact;

	public int factorial1(int num) {
		int n = num;
		int fact = 1;
		int i = 1;
		do {
			int fact1 = fact * i;
			fact = fact1;
			i++;
		} while (i <= n);
		return fact;
	}

}
