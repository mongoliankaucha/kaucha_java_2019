package com.cerotid.bank.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;

public class BankTester {
	public static void main(String[] args) throws IOException {

		Bank bank = new Bank();
		Customer customer1 = new Customer();
		Account customer1acct1 = new Account();
		customer1acct1.setAccountType("Checking");

		Account customer1acct2 = new Account();
		customer1acct2.setAccountType("Business Checking");

		Account customer1acct3 = new Account();
		customer1acct3.setAccountType("Saving");

		ArrayList<Account> customer1Accounts = new ArrayList<>();
		customer1Accounts.add(customer1acct1);
		customer1Accounts.add(customer1acct2);
		customer1Accounts.add(customer1acct3);

		customer1.setAddress("Irving,TX");
		customer1.setFirstName("Himal");
		customer1.setLastName("Kaucha");

		customer1.setAccounts(customer1Accounts);

		System.out.println();

		Customer customer2 = new Customer();

		Customer customer3 = new Customer();

		ArrayList<Customer> customerList = new ArrayList<>();
		customerList.add(customer1);
		customerList.add(customer2);
		customerList.add(customer3);

		bank.setCustomers(customerList);
		bank.printBankName();
		bank.printBankDetails();

		CustomerReader csr = new CustomerReader();
		csr.readFile();

		csr.writeFile();

	}

}
