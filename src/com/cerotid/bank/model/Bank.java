package com.cerotid.bank.model;

import java.util.ArrayList;

public class Bank {
	
	private final String bankName="Bank of America";
	private ArrayList<Customer> customers;
	
	//Required Accessor methods and Modifier method created
	
	public ArrayList<Customer> getCustomers() {
		return customers;
	}
	public void setCustomers(ArrayList<Customer> customers) {
		this.customers = customers;
	}
	public String getBankName() {
		return bankName;
	}
	
	
	public void printBankName() {
		//TODO ADD bank name logic here
		System.out.println(getBankName());
	}
	 public void printBankDetails(){
		//TODO ADD bank details logic
		 System.out.println(toString());
	}
	@Override
	public String toString() {
		return "Bank [bankName=" + bankName + ", customers=" + customers + "]";
	}
	 
	 
}
