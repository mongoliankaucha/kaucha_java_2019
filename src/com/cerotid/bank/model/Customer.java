package com.cerotid.bank.model;

import java.util.ArrayList;
import java.util.Comparator;

public class Customer {
	private String firstName;
	private String lastName;
	private String address;
	private ArrayList<Account> accounts;

	public Customer() {

	}

	public Customer(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}

	// Accessors Mutator

	public String getAddress() {
		return address;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public ArrayList<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(ArrayList<Account> accounts) {
		this.accounts = accounts;
	}

	void printCustomerAccount() {

		// TODO print customer Accounts
	}

	void printCustomerDetails() {
		// TODO print customer Details
		System.out.println(toString());
	}

	@Override
	public String toString() {
		return "Customer [firstName=" + firstName + ", lastName=" + lastName + ", address=" + address + ", accounts="
				+ accounts + "]";
	}
	/*public static Comparator<Customer> LastNameComparator1 = new Comparator<Customer>() {

		@Override
		public int compare(Customer arg0, Customer arg1) {
			// TODO Auto-generated method stub
			return 0;
		}
		
	};*/

	public static Comparator<Customer> LastNameComparator = new Comparator<Customer>() {

		@Override
		public int compare(Customer o1, Customer o2) {
			// TODO Auto-generated method stub
			if(o1.getLastName().compareTo(o2.getLastName())==0) {
				return o1.getFirstName().compareTo(o2.getFirstName());
			}
			return o1.getLastName().compareTo(o2.getLastName());
		}
	};
	
	public static Comparator<Customer> FirstNameComparator = new Comparator<Customer>() {

		@Override
		public int compare(Customer o1, Customer o2) {
			// TODO Auto-generated method stub
			if(o1.getFirstName().compareTo(o2.getFirstName())==0) {
				return o1.getLastName().compareTo(o2.getLastName());
			}
			return o1.getFirstName().compareTo(o2.getFirstName());
		}
	};

}
