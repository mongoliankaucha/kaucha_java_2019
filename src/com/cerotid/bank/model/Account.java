package com.cerotid.bank.model;

public class Account {

	private String accountType;
	
	

	public String getAccountType() {
		return accountType;
	}



	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}



	public void printAccountInfo() {
		//TODO print AccountInfo
		System.out.println(toString());
	}



	@Override
	public String toString() {
		return "Account [accountType=" + accountType + "]";
	}
	
	
}
