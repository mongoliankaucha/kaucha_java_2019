package com.calculator.java;

public interface CalculatorInterface {
	public void addCalc(double a, double b);

	public void subCalc(double a, double b);

	public void mulCalc(double a, double b);

	public void divCalc(double a, double b);

}
