package com.calculator.java;

import java.util.Scanner;

public class CalculatorTestClass {
	private static CalculatorInterface calculatorInterface;
	static double a;
	static double b;
	static String s;

	public static void main(String[] args) {
		CalculatorTestClass calculatorTestClass = new CalculatorTestClass();
		calculatorTestClass.setCalculatorInterface(new CalculatorImplements());

		while (true) {
			readData();
			actionMenuPerform(a, s, b);
		}

	}

	private static void actionMenuPerform(double a, String s, double b) {
		// TODO Auto-generated method stub
		while (true) {
			try {
				switch (s) {
				case "+":
					calculatorInterface.addCalc(a, b);
					break;
				case "-":
					calculatorInterface.subCalc(a, b);
					break;
				case "*":
					calculatorInterface.mulCalc(a, b);
					break;
				case "/":
					calculatorInterface.divCalc(a, b);
					break;
				case "exit":
					System.exit(0);
					break;
				default:
					break;
				}
				break;

			} catch (Exception e) {
				System.out.println("error");
			}
		}

	}

	private static void readData() {
		// TODO Auto-generated method stub
		System.out.println("Start");
		a = getScanner().nextInt();
		s = getScanner().nextLine();
		b = getScanner().nextInt();

	}

	private static Scanner getScanner() {
		// TODO Auto-generated method stub
		return new Scanner(System.in);
	}

	public CalculatorInterface getCalculatorInterface() {
		return calculatorInterface;
	}

	public void setCalculatorInterface(CalculatorInterface calculatorInterface) {
		this.calculatorInterface = calculatorInterface;
	}

}
