package primeNumber;

public class PrimeInterfaceImplement implements PrimeInterface {

	int sum = 1;

	@Override
	public void primeNumberFinderForLoop(int lowerLimit, int upperLimit) {

		// TODO Auto-generated method stub
		lineDisplay(lowerLimit, upperLimit);
		for (int i = lowerLimit; i <= upperLimit; i++) {
			int count = 0;
			for (int j = 1; j <= i; j++) {

				if (i % j == 0) {
					count = count + 1;
				}

			}
			printPrimeNumber(count, i);

		}
		sum = 1;
	}

	@Override
	public void primeNumberFinderWhileLoop(int lowerLimit, int upperLimit) {
		// TODO Auto-generated method stub
		lineDisplay(lowerLimit, upperLimit);
		int i = lowerLimit;

		while (i <= upperLimit) {
			int count = 0;
			int j = 1;
			while (j <= i) {
				if (i % j == 0) {
					count = count + 1;
				}
				j++;

			}
			printPrimeNumber(count, i);
			i++;

		}
		sum = 1;

	}

	@Override
	public void primeNumberFinderForWhileLoop(int lowerLimit, int upperLimit) {
		// TODO Auto-generated method stub
		lineDisplay(lowerLimit, upperLimit);
		for (int i = lowerLimit; i <= upperLimit; i++) {
			int count = 0;
			int j = 1;
			while (j <= i) {
				if (i % j == 0) {
					count = count + 1;
				}
				j++;
			}
			printPrimeNumber(count, i);
		}
		sum = 1;
	}

	@Override
	public void primeNumberFinderWhileForLoop(int lowerLimit, int upperLimit) {
		// TODO Auto-generated method stub
		lineDisplay(lowerLimit, upperLimit);
		int i = lowerLimit;

		while (i <= upperLimit) {
			int count = 0;
			for (int j = 1; j <= i; j++) {
				if (i % j == 0) {
					count = count + 1;
				}

			}
			printPrimeNumber(count, i);
			i++;

		}
		sum = 1;
	}

	private void lineDisplay(int lowerLimit, int upperLimit) {
		// TODO Auto-generated method stub
		System.out.println("Prime numbers between " + lowerLimit + " and " + upperLimit + " are:");
	}

	private void printPrimeNumber(int count, int i) {
		// TODO Auto-generated method stub
		if (count == 2) {
			if (sum % 10 != 0) {
				System.out.print(i + " ");
				sum = sum + 1;
			} else {
				System.out.println(i + " ");
				sum = sum + 1;
			}

		}

	}

}
