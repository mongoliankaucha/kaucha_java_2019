package primeNumber;

public interface PrimeInterface {
	public void primeNumberFinderForLoop(int lowerLimit, int upperLimit);

	public void primeNumberFinderWhileLoop(int lowerLimit, int upperLimit);

	public void primeNumberFinderForWhileLoop(int lowerLimit, int upperLimit);

	public void primeNumberFinderWhileForLoop(int lowerLimit, int upperLimit);

}
