package primeNumber;

import java.util.Scanner;

public class PrimeNumberTester {
	private static PrimeInterface primeInterface;

	public static void main(String[] args) {
		PrimeNumberTester primeNumberTester = new PrimeNumberTester();
		primeNumberTester.setPrimeInterface(new PrimeInterfaceImplement());

		while (true) {
			displayMenu();
			actionMenuPerform();
		}

	}

	private static void actionMenuPerform() {
		// TODO Auto-generated method stub

		while (true) {
			try {
				switch (getScanner().nextInt()) {
				case 1:
					primeInterface.primeNumberFinderForLoop(getLowerLimit(), getUpperLimit());
					break;

				case 2:
					primeInterface.primeNumberFinderWhileLoop(getLowerLimit(), getUpperLimit());
					break;

				case 3:
					primeInterface.primeNumberFinderForWhileLoop(getLowerLimit(), getUpperLimit());
					break;

				case 4:
					primeInterface.primeNumberFinderWhileForLoop(getLowerLimit(), getUpperLimit());
					break;

				case 0:
					System.out.println("Closed !!!");
					System.exit(0);
					break;
				default:
					break;
				}
				break;
			} catch (Exception e) {
				System.out.println("error");

			}
		}

	}

	private static void displayMenu() {
		// TODO Auto-generated method stub
		System.out.println();
		System.out.println("Prime number finder program:");
		System.out.println("enter 1 to use for-for loop");
		System.out.println("enter 2 to use while-while loop");
		System.out.println("enter 3 to use while-for loop");
		System.out.println("enter 4 to use for-while loop");
		System.out.println("enter 0 to exit");

	}

	private static int getUpperLimit() {
		System.out.println("enter upper limit");
		return getScanner().nextInt();
	}

	private static int getLowerLimit() {
		System.out.println("enter lower limit");
		return getScanner().nextInt();
	}

	private static Scanner getScanner() {
		return new Scanner(System.in);
	}

	public PrimeInterface getPrimeInterface() {
		return primeInterface;
	}

	public void setPrimeInterface(PrimeInterface primeInterface) {
		this.primeInterface = primeInterface;
	}

}
